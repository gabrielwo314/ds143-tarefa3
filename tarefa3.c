#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct time {
  double timestamp;   // 0 - 86400
};

typedef struct time t_time;

struct timeelement{
  struct time key;
  char* value;
  struct timeelement* next;
};

typedef struct timeelement t_timeelement;

struct timetable {
  struct timeelement* first;
  struct timeelement* last;
  int size;
};

typedef struct timetable t_timetable;

int time_cmp(t_time h1, t_time h2){
  if(h1.timestamp > h2.timestamp) return 1;
  else if(h1.timestamp < h2.timestamp) return -1;
  else return 0;
}
// 900003
// 09:00:03
char* convertTimeToString(t_time time){
  int t = time.timestamp;
  char* time_str = malloc(sizeof(char) *9);
  char* time_str2 = malloc(sizeof(char) *9);
  sprintf(time_str, "%d", t);

  if(time.timestamp < 100000){
    time_str2[0] = '0';
    time_str2[1] = '\0';
  }
  strcat(time_str2, time_str);

  time_str[0] = time_str2[0];
  time_str[1] = time_str2[1];
  time_str[2] = ':';
  time_str[3] = time_str2[2];
  time_str[4] = time_str2[3];
  time_str[5] = ':';
  time_str[6] = time_str2[4];
  time_str[7] = time_str2[5];
  time_str[8] = '\0';
  
  return time_str;
}

t_time convertStringToTime(char* time_str){
  t_time t;
  char s[6] = "";

  for(int i = 0; i < 8; i++)
    if(time_str[i] != ':')
      strncat(s, &(time_str[i]), 1);
  
  t.timestamp = atoi(s);
  return t;
}

// --------------------------------------- TIME TABLE FUNCIONS --------------------------------------- //

void put(t_timetable* table, t_time key, char * value){ 
  if(table == NULL) return;

  t_timeelement* new_elem = (t_timeelement*) malloc(sizeof(t_timeelement));
  new_elem->key = key;
  char* v = malloc(sizeof(char) * 20);
  strcpy(v, value);
  new_elem->value = v;

  if(table->first == NULL){     // está vazia? vira o 1o e o último
    new_elem->next = NULL;
    table->first = new_elem;
    table->last = new_elem;
    table->size++;
    return;
  }

  t_timeelement* elem = table->first;

  if(new_elem->key.timestamp < elem->key.timestamp){    // é menor que o primeiro elemento? vira o 1o
    new_elem->next = elem;
    table->first = new_elem;
    table->size++;
    return;
  }

  while(elem->next != NULL){
    if(new_elem->key.timestamp > elem->next->key.timestamp){    // é maior que o próximo? avança
      elem = elem->next;
    } else {                                                   // insere no meio
      new_elem->next = elem->next;
      elem->next = new_elem;
      table->size++;
      return;
    }
  }

  new_elem->next = elem->next;
  elem = table->last;                               // insere no final
  elem->next = new_elem;
  table->last = new_elem;
  table->size++;
  return;
} /////////////////////////////////////////////////////////////////// put

char* get(t_timetable* table, t_time key){
  t_timeelement* elem = table->first;
  while(elem != NULL){
    if(elem->key.timestamp == key.timestamp){
      return elem->value;
    }
    elem = elem->next;
  }
  return NULL;
} ///////////////////////////////////////////////////////////////////  get

void delete(t_timetable* table, t_time key){
  if(table == NULL || table->first == NULL) return;

  t_timeelement *elem = table->first, *temp;

  if(elem->key.timestamp == key.timestamp){
    if(elem == table->last)
      table->last = NULL;
    table->first = elem->next;
    free(elem);
    table->size--;
    return;
  }

  while(elem->next != NULL && elem->next->key.timestamp != key.timestamp)
    elem = elem->next;

  if(elem->next == NULL) return;
  if(elem->next == table->last)
    table->last = elem;
  temp = elem->next;
  elem->next = temp->next;
  free(temp);
  table->size--;
  return;

} ///////////////////////////////////////////////////////////////////  delete

int contains(t_timetable* table, t_time key){
  t_timeelement* elem = table->first;
  while(elem != NULL){
    if(elem->key.timestamp == key.timestamp)
      return 1;
    elem = elem->next;
  }
  return 0;
}

int is_empty(t_timetable* table){
  return !table->size;
}

int size(t_timetable* table){
  return table->size;
}

t_time min(t_timetable* table){
  return table->first->key;
}

t_time max(t_timetable* table){
  return table->last->key;
}

t_time t_floor(t_timetable* table, t_time key){
  t_timeelement* elem = table->first;
  for(int i = 0; elem->next != NULL; i++){
    if(elem->next->key.timestamp > key.timestamp)
      return elem->key;
    elem = elem->next;
  }
  return elem->key;
}

t_time ceiling(t_timetable* table, t_time key){
  t_timeelement* elem = table->first;
  for(int i = 0; elem->next != NULL; i++){
    if(elem->key.timestamp >= key.timestamp)
      return elem->key;
    elem = elem->next;
  }
}

int rank(t_timetable* table, t_time key){
  t_timeelement* elem = table->first;
  int i = 0;
  for(i = 0; elem->next != NULL; i++){
    if(elem->key.timestamp >= key.timestamp)
      break;
    elem = elem->next;
  }
  return i;
}

t_time t_select(t_timetable* table, int k){
  t_timeelement* elem = table->first;
  for(int i = 0; elem->next != NULL; i++){
    if(k == i+1)
      return elem->key;
    elem = elem->next;
  }

}

void delete_min(t_timetable* table){
  delete(table, table->first->key);
}

void delete_max(t_timetable* table){
  delete(table, table->last->key);
}

int size_range(t_timetable* table, t_time lo, t_time hi){
  t_timeelement* elem = table->first;
  int n = 0;
  for(int i = 0; elem != NULL; i++){
    if(elem->key.timestamp >= hi.timestamp)
      return n;
    if(elem->key.timestamp > lo.timestamp)
      n++;
    elem = elem->next;
  }
  return n;
}

t_time* keys(t_timetable* table, t_time lo, t_time hi){
  t_timeelement* elem = table->first;
  int n = 0;
  t_time* keys = malloc(sizeof(t_time) * table->size);
  for(int i = 0; elem != NULL; i++){
    if(elem->key.timestamp >= hi.timestamp)
      return keys;
    if(elem->key.timestamp > lo.timestamp){
      keys[n] = elem->key; 
      n++;
    }
    elem = elem->next;
  }
  return keys;
}

void print_table(t_timetable* table){
  t_timeelement* elem = table->first;
  for(int i = 0; elem != NULL; i++){
    printf("%s, %s\n", convertTimeToString(elem->key), elem->value);
    elem = elem->next;
  }
}

int main(){

  t_timetable* table = (t_timetable*) malloc(sizeof(t_timetable));

  table->first = NULL;
  table->last = NULL;
  table->size = 0;

  if(is_empty(table)) printf("Table is empty!\n\n");
  else printf("Table is not empty!\n\n");

  char* keyStr;
  char* value;
  char buff[1024];
  FILE *file = fopen("data.txt", "r");
  while(fgets(buff, 1024, file)) {
    keyStr = strtok(buff, " ");
    value = strtok(NULL, " ");
    put(table, convertStringToTime(keyStr), value);
  }

  if(is_empty(table)) printf("Table is empty!\n\n");
  else printf("Table is not empty!\n\n");

  printf("Size: %d\n\n", size(table));

  keyStr = "09:00:13";
  printf("get(09:00:13): %s\n\n", get(table, convertStringToTime(keyStr)));

  t_time a = min(table);

  printf("min(): %s\n", convertTimeToString(a));
  printf("max(): %s\n\n", convertTimeToString(max(table)));

  keyStr = "09:30:00";
  printf("floor(09:30:00): %s\n", convertTimeToString(t_floor(table, convertStringToTime(keyStr))));
  keyStr = "09:20:00";
  printf("ceiling(09:20:00): %s\n\n", convertTimeToString(ceiling(table, convertStringToTime(keyStr))));

  keyStr = "09:25:00";
  printf("rank(09:25:00): %d\n\n", rank(table, convertStringToTime(keyStr)));

  printf("select(4): %s\n\n", convertTimeToString(t_select(table, 4)));

  fclose(file);
  printf("\n\nFim do código\n\n\n");
}
